import { Categorie } from '../modeles/categorie';

export class ProduitDto {
    public id:number;
    public nom:string;
    public prix:number;
    public categorie:Categorie;
    public show:boolean;
}
