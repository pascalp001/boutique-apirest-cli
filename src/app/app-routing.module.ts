import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProduitListComponent } from './composants/produit-list/produitList.component';
import { ProduitFormComponent } from './composants/produit-form/produit-form.component';
import { ProduitEditComponent } from './composants/produit-edit/produit-edit.component';
import { AccueilComponent } from './composants/accueil/accueil.component'
import { ProduitsResolver } from './services/produits.resolver';

const routes: Routes = [
  { path: 'accueil', component: AccueilComponent },
  { path: 'produits', component: ProduitListComponent },
  { path: 'produitsres', component: ProduitListComponent },
  { path: 'addproduit', component: ProduitFormComponent },
  { path: 'edit-produit/:id', component: ProduitEditComponent },
  { path: '', redirectTo: '/accueil', pathMatch: 'full'  },
  { path: '**', redirectTo: '/accueil', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [ProduitsResolver],
  exports: [RouterModule]
})
export class AppRoutingModule { }
