import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { ProduitService } from 'src/app/services/produit.service';
import { ProduitDto } from '../modeles/produit-dto';

@Injectable({
  providedIn: 'root'
})
export class ProduitsResolver implements Resolve<ProduitDto[]> {
  constructor(private produitService:ProduitService, private router:Router) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProduitDto[]> {
    return this.produitService.getAll();
  }
}
