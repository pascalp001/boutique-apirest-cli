import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Categorie } from '../modeles/categorie';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*' })
  };
const baseUrl = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {  
    return this.http.get(baseUrl+"/categories",httpOptions); 
  }
 
  get(id): Observable<any> {
    return this.http.get(`${baseUrl}/categorie/${id}`);
  }

  create(data): Observable<any> {
    return this.http.post(baseUrl+"/categorie", data);
  }

  update(id, data): Observable<any> {
    return this.http.put(`${baseUrl}/categorie/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${baseUrl}/categorie/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl+"/categorie");
  }

  findByNom(nom): Observable<any> {
    return this.http.get(`${baseUrl}/categorie?nom=${nom}`);
  }
}
