import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Produit } from '../modeles/produit';

const baseUrl = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
 
  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {  
    return this.http.get(baseUrl+"/produits"); 
  }
 
  get(id): Observable<any> {
    return this.http.get(`${baseUrl}/produit/${id}`);
  }

  create(data): Observable<any> {
    return this.http.post(baseUrl+"/produit", data);
  }

  update(id, data): Observable<any> {
    return this.http.put(`${baseUrl}/produit/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${baseUrl}/produit/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl+"/produits");
  }

  findByNom(nom): Observable<any> {
    return this.http.get(`${baseUrl}/produit?nom=${nom}`);
  }
  findById(id): Observable<any> {
    return this.http.get(`${baseUrl}/produit/${id}`);
  }

}
