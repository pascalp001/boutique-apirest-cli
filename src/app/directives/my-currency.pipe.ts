import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myCurrency'})
export class MyCurrencyPipe implements PipeTransform {

  private DECIMAL_SEPARATOR: string= ".";
  private THOUSANDS_SEPARATOR: string= " ";
  private PADDING:string= "000000";

  transform(value: number | string, fractionSize: number = 2): string {
    //séparation de value entre entier et fraction=la partie décimale
    let [ entier, fraction = "" ] = (value || "").toString().split(this.DECIMAL_SEPARATOR);

    //on ne conserve que 2 chiffres dans la partie décimale
    fraction = fractionSize > 0
      ? this.DECIMAL_SEPARATOR + (fraction + this.PADDING).substring(0, fractionSize)
      : "";

    //on met un séparateur pour les milliers
    entier = entier.replace(/\B(?=(\d{3})+(?!\d))/g, this.THOUSANDS_SEPARATOR);

    return entier + fraction;
  }

  //Opération inverse
  parse(value: string, fractionSize: number = 2): string {
    let [ entier, fraction = "" ] = (value || "").split(this.DECIMAL_SEPARATOR);

    entier = entier.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");

    fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
      ? this.DECIMAL_SEPARATOR + (fraction + this.PADDING).substring(0, fractionSize)
      : "";

    return entier + fraction;
  }
}
