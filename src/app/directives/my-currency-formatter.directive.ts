import { Directive, HostListener, ElementRef, OnInit } from '@angular/core';
import { MyCurrencyPipe } from "./my-currency.pipe";

@Directive({
  selector: '[MyCurrencyFormatter]'
})
export class MyCurrencyFormatterDirective implements OnInit{

  private el: HTMLInputElement;
  //private currencyPipe: MyCurrencyPipe;

  constructor(private elementRef: ElementRef, private currencyPipe: MyCurrencyPipe ) {  this.el = this.elementRef.nativeElement; }

  ngOnInit() {
    alert("init");
    this.el.value = this.currencyPipe.transform(this.el.value);
  }
  @HostListener("focus", ["$event.target.value"])
  onFocus(value) {
    this.el.value = this.currencyPipe.parse(value); //affichage brut
  }

  @HostListener("blur", ["$event.target.value"])
  onBlur(value) {
    this.el.value = this.currencyPipe.transform(value); //affichage formaté
  }
  @HostListener('mouseenter', ['$event']) 
  onMouseEnter(event: Event) {
    console.log('mouseenter');
    console.log(event);
  }

  @HostListener('mouseleave', ['$event']) 
  onMouseLeave(event: Event) {
    console.log('mouseleave');
    console.log(event);
  }
}
