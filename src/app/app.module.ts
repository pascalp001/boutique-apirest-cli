import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MenuComponent } from './composants/menu/menu.component';
import { ProduitListComponent } from './composants/produit-list/produitList.component';
import { ProduitFormComponent } from './composants/produit-form/produit-form.component';
import { AccueilComponent } from './composants/accueil/accueil.component';
import { ProduitEditComponent } from './composants/produit-edit/produit-edit.component';
import { ProduitService } from './services/produit.service';
import { CategorieService } from './services/categorie.service';
import { MyCurrencyFormatterDirective } from './directives/my-currency-formatter.directive';
import { MyCurrencyPipe } from './directives/my-currency.pipe';
import { CategorieFormComponent } from './composants/categorie-form/categorie-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ProduitListComponent,
    ProduitFormComponent,
    AccueilComponent,
    ProduitEditComponent,
    MyCurrencyFormatterDirective,
    MyCurrencyPipe,
    CategorieFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ProduitService,CategorieService,MyCurrencyFormatterDirective,MyCurrencyPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
