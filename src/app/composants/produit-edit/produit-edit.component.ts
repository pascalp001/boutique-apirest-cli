import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Produit } from 'src/app/modeles/produit';
import { ProduitService } from 'src/app/services/produit.service';
import { Categorie } from 'src/app/modeles/categorie';
import { CategorieService } from 'src/app/services/categorie.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MyCurrencyFormatterDirective } from 'src/app/directives/my-currency-formatter.directive';
import { MyCurrencyPipe } from 'src/app/directives/my-currency.pipe';

@Component({
  selector: 'app-produit-edit',
  templateUrl: './produit-edit.component.html',
  styleUrls: ['./produit-edit.component.scss']
})
export class ProduitEditComponent implements OnInit {

  produit:Produit;
  showRes:boolean=false;
  categories:any[];
  showAddCategory:boolean=false;
  categorie:Categorie;
  autre:string="Autre";
  @ViewChild("addCateg") el1:ElementRef;
  @ViewChild("categAdd") el2:ElementRef;
 
  constructor(private pService:ProduitService, private cService:CategorieService, private router:Router, private route:ActivatedRoute){
    this.produit = new Produit();
  }
  ngOnInit() {
    this.cService.getAll().subscribe(elts => {this.categories=elts;});
    /*let produitId = window.localStorage.getItem("editProduitId");
    if(!produitId) {
      alert("Invalid action.")
      this.router.navigate(['/produits']);
      return;
    }*/
    this.route.paramMap.subscribe(
      prm=>{
        const produitId = prm.get('id');
        this.pService.findById(produitId)
        .subscribe( data => {
          this.produit = data;
        });
    });
    
  }

  onSubmit(form:NgForm) { 
    this.produit.id = form.value['id'];
    this.produit.nom = form.value['nom'];
    this.produit.prix = form.value['prix'];
    this.produit.id_categorie = form.value['categorie'];
    if(this.autre != "Autre"){
      this.categorie={id:null,nom:this.autre};
      this.cService.create(this.categorie)
      .subscribe(res1=>{
        console.log(res1);
        this.categorie=res1;
        console.log('ici categorie.id = '+this.categorie.id);
        this.produit.id_categorie=this.categorie.id;
        this.pService.update(this.produit.id,this.produit)
        .subscribe(
          res2=>{
            console.log('modification réussie'+res2.id);
            this.showRes=true;
            }
          );
      });
    }
    else{
      this.pService.update(this.produit.id,this.produit)
      .subscribe(
        res=>{
          console.log('modification '+this.produit.nom+" terminé ; id="+res.id);
          this.showRes=true;
          }
        );
    }

  }
  showAddCategorie(){
    this.showAddCategory=true;
  }
  fermeAddCategorie(){
    this.el2.nativeElement.text=this.el1.nativeElement.value;
    this.autre=this.el1.nativeElement.value;
    //document.getElementById("categAdd").value = document.getElementById("addCateg").value;
    //$('#categAdd').val($('#addCateg').val());
    this.showAddCategory=false;
  }
  onSubmitCat(){
    this.cService.create(this.categorie)
    .subscribe(
      res=>{
        console.log('categorie nouvelle'+res.nom);
        this.cService.getAll().subscribe(elts => {this.categories=elts;});
        }
      );
  }
  


}
