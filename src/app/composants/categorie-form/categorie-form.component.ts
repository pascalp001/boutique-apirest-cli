import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Categorie } from 'src/app/modeles/categorie';
import { CategorieService } from 'src/app/services/categorie.service';

declare var $:any;
@Component({
  selector: 'add-categorie',
  template: `&#10157;
  <input type="text" id="categ" class="formH len200" [(ngModel)]="categorieName" placeholder="Nom nouvelle catégorie" />
  <button type="button" (click)="addCategorie()" class="formH">Valider</button>
  <div class="formH" id="result"></div>`,
  styles: ['.formH{display:inline-block;} .len200 {width: 200px;}']
})


export class CategorieFormComponent implements OnInit {
  @Output() 
  categorieEvent = new EventEmitter();
  categorie:Categorie = new Categorie;
  categorieName:string;

  constructor(private cService:CategorieService ) {}

  ngOnInit(): void {
  }
  addCategorie(){
    this.categorie={id:null,nom:this.categorieName};
    this.cService.create(this.categorie)
    .subscribe(
      res=>{
        console.log('categorie nouvelle : '+res.nom+', id='+res.id);
        this.categorie.id=res.id;
        this.categorieEvent.emit(this.categorie);
        }
      );
    
  }
}
