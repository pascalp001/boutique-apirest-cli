import { Component, OnInit } from '@angular/core';
import { ProduitDto } from 'src/app/modeles/produit-dto';
import { Categorie } from 'src/app/modeles/categorie';
import { ProduitService } from 'src/app/services/produit.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var $:any;
@Component({
  selector: 'produit-list',
  templateUrl: './produitList.component.html',
  styleUrls: ['./produitList.component.scss']
})
export class ProduitListComponent implements OnInit {

  //c:Categorie={id:1,nom:"legume"}
  produits:ProduitDto[]=[{id:1,nom:'chou',prix:2.50, categorie:{id:1,nom:"legume"}, show:null}];

  constructor(private produitService:ProduitService, private router:Router) {
  }

  ngOnInit(): void {
    this.produitService.getAll()
    .subscribe(elts => {   
      this.produits=elts;
      for(let i = 0; i<this.produits.length; i++){
        //$("#sup"+this.produits[i].id).css("display","none");
        //this.produits[i].show = false; //confirmation suppression
      }
    });
  }
  delProduitOk(p){
    this.produitService.delete(p)
      .subscribe(()=>{
        console.log('suppression effectuée');
        window.location.reload();
      });
  }
  toggle(p){
    p.show = !p.show;
    if(p.show){
      $("#sup"+p.id).css("display","block");
      $("#sup"+p.id).animate({opacity:1, left:"-=150"},300);
    }
    else{
      $("#sup"+p.id).animate({opacity:0, left:"+=150"},300, function(){$(this).css("display","none");});
    }
  }
 /* change(p){
    this.toggle(p);
  }*/
 /* editProduit(p){
    window.localStorage.removeItem("editProduitId");
    window.localStorage.setItem("editProduitId", p.id.toString());
    console.log("edit prêt : "+p.id.toString());
    this.router.navigate(['edit-produit']);
  }*/
  editProduit(p){
    console.log("edit prêt : "+p.id.toString());
    this.router.navigate(['edit-produit', p.id]);
  }
}
