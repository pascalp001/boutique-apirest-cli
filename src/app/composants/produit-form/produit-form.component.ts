import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Produit } from 'src/app/modeles/produit';
import { ProduitService } from 'src/app/services/produit.service';
import { CategorieService } from 'src/app/services/categorie.service';
import { Categorie } from 'src/app/modeles/categorie';
import { NgForm } from '@angular/forms';

declare var $:any;
@Component({
  selector: 'produit-form',
  templateUrl: './produit-form.component.html',
  styleUrls: ['./produit-form.component.scss']
})
export class ProduitFormComponent implements OnInit{

  produit:Produit;
  showRes:boolean=false;
  categories:any[];
  showAddCategory:boolean=false;
  categorie:Categorie;
 
  constructor(private pService:ProduitService, private cService:CategorieService){
    this.produit = new Produit();
  }
  ngOnInit(): void {
    this.cService.getAll().subscribe(elts => {this.categories=elts;});
  }

  onSubmit(form:NgForm) { 
    this.produit.nom = form.value['nom'];
    this.produit.prix = form.value['prix'];
    this.produit.id_categorie = form.value['categorie'];

    this.pService.create(this.produit)
    .subscribe(
      res=>{
        console.log('Enregistrement '+this.produit.nom+" terminé ; id="+res.id);
        this.showRes=true;
        }
      );
  }

  showAddCategorie(){
    this.showAddCategory=true;
    $(".btnPlus").css({"background":"#cFc", "color":"green", "font-weight":"700"});
    $(".addCategShow").animate({"margin-left":"10px"},500);
  }

  ajouteCategorie(nvCateg){
    $( "#categorie" ).append( '<option value="'+nvCateg.id+'">'+nvCateg.nom+'</option>');
    $(".btnPlus").css({"background":"#a00c", "color":"#33", "font-weight":"700"});		
    $(".addCategShow").animate({"margin-left":"-1000px"},500);
  }
}
