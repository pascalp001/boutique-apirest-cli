import { Component, OnInit } from '@angular/core';

declare var $ :any;
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  constructor() {  
  }
  img:string="../assets/images/LogoProG8.PNG";
  toggle:boolean=false;
  toggleImage(){
    $("#toggleImg").slideToggle( 500,"swing");
    this.toggle=!this.toggle;
    if(this.toggle){
      $("#btnImg").css({"background":"#ca3","font-weight":"700","color":"white"});
    }
    else{
      $("#btnImg").css({"background":"#cfc","font-weight":"400","color":"#222"});
    }
    
  }
  ngOnInit(): void {
    $("#btnImg").css({"background":"#cfc","padding":"10px 20px","border-radius":"10px"});
  }

}
